<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:32e2f093-5b98-4192-abd5-9a67bb0f36a1(MySolution.myModel)">
  <persistence version="9" />
  <languages>
    <use id="340b79db-f74f-4ba0-8875-4d776d5e3a88" name="MyLanguage" version="-1" />
    <use id="798100da-4f0a-421a-b991-71f8c50ce5d2" name="jetbrains.mps.build" version="0" />
    <use id="698a8d22-a104-47a0-ba8d-10e3ec237f13" name="jetbrains.mps.build.workflow" version="0" />
    <use id="0cf935df-4699-4e9c-a132-fa109541cba3" name="jetbrains.mps.build.mps" version="7" />
  </languages>
  <imports>
    <import index="ffeo" ref="r:874d959d-e3b4-4d04-b931-ca849af130dd(jetbrains.mps.ide.build)" />
    <import index="arit" ref="r:0d66e868-9778-4307-b6f9-4795c00f662f(jetbrains.mps.build.workflow.preset.general)" implicit="true" />
  </imports>
  <registry>
    <language id="698a8d22-a104-47a0-ba8d-10e3ec237f13" name="jetbrains.mps.build.workflow">
      <concept id="2769948622284546677" name="jetbrains.mps.build.workflow.structure.BwfSubTask" flags="ng" index="2VaFvH">
        <child id="2769948622284606050" name="statements" index="2VaTZU" />
      </concept>
      <concept id="2769948622284768359" name="jetbrains.mps.build.workflow.structure.BwfAntStatement" flags="ng" index="2Vbh7Z">
        <child id="2769948622284768360" name="element" index="2Vbh7K" />
      </concept>
      <concept id="3961775458390032824" name="jetbrains.mps.build.workflow.structure.BwfTaskPart" flags="ng" index="3bMsLL">
        <reference id="3961775458390032825" name="task" index="3bMsLK" />
        <child id="3961775458390032826" name="subTasks" index="3bMsLN" />
      </concept>
    </language>
    <language id="479c7a8c-02f9-43b5-9139-d910cb22f298" name="jetbrains.mps.core.xml">
      <concept id="6666499814681541919" name="jetbrains.mps.core.xml.structure.XmlTextValue" flags="ng" index="2pMdtt">
        <property id="6666499814681541920" name="text" index="2pMdty" />
      </concept>
      <concept id="6666499814681415858" name="jetbrains.mps.core.xml.structure.XmlElement" flags="ng" index="2pNNFK">
        <property id="6666499814681415862" name="tagName" index="2pNNFO" />
        <property id="6999033275467544021" name="shortEmptyNotation" index="qg3DV" />
        <child id="6666499814681415861" name="attributes" index="2pNNFR" />
        <child id="1622293396948928802" name="content" index="3o6s8t" />
      </concept>
      <concept id="6666499814681447923" name="jetbrains.mps.core.xml.structure.XmlAttribute" flags="ng" index="2pNUuL">
        <property id="6666499814681447926" name="attrName" index="2pNUuO" />
        <child id="6666499814681541918" name="value" index="2pMdts" />
      </concept>
    </language>
    <language id="798100da-4f0a-421a-b991-71f8c50ce5d2" name="jetbrains.mps.build">
      <concept id="5481553824944787378" name="jetbrains.mps.build.structure.BuildSourceProjectRelativePath" flags="ng" index="55IIr" />
      <concept id="2755237150521975431" name="jetbrains.mps.build.structure.BuildVariableMacroInitWithString" flags="ng" index="aVJcg">
        <child id="2755237150521975437" name="value" index="aVJcq" />
      </concept>
      <concept id="7321017245476976379" name="jetbrains.mps.build.structure.BuildRelativePath" flags="ng" index="iG8Mu">
        <child id="7321017245477039051" name="compositePart" index="iGT6I" />
      </concept>
      <concept id="3767587139141066978" name="jetbrains.mps.build.structure.BuildVariableMacro" flags="ng" index="2kB4xC">
        <child id="2755237150521975432" name="initialValue" index="aVJcv" />
      </concept>
      <concept id="4993211115183325728" name="jetbrains.mps.build.structure.BuildProjectDependency" flags="ng" index="2sgV4H">
        <reference id="5617550519002745380" name="script" index="1l3spb" />
        <child id="4129895186893471026" name="artifacts" index="2JcizS" />
      </concept>
      <concept id="4380385936562003279" name="jetbrains.mps.build.structure.BuildString" flags="ng" index="NbPM2">
        <child id="4903714810883783243" name="parts" index="3MwsjC" />
      </concept>
      <concept id="6057319140845467763" name="jetbrains.mps.build.structure.BuildSource_JavaLibrary" flags="ng" index="PiPfp" />
      <concept id="8618885170173601777" name="jetbrains.mps.build.structure.BuildCompositePath" flags="nn" index="2Ry0Ak">
        <property id="8618885170173601779" name="head" index="2Ry0Am" />
        <child id="8618885170173601778" name="tail" index="2Ry0An" />
      </concept>
      <concept id="6647099934206700647" name="jetbrains.mps.build.structure.BuildJavaPlugin" flags="ng" index="10PD9b" />
      <concept id="7389400916848136194" name="jetbrains.mps.build.structure.BuildFolderMacro" flags="ng" index="398rNT">
        <child id="7389400916848144618" name="defaultPath" index="398pKh" />
      </concept>
      <concept id="7389400916848153117" name="jetbrains.mps.build.structure.BuildSourceMacroRelativePath" flags="ng" index="398BVA">
        <reference id="7389400916848153130" name="macro" index="398BVh" />
      </concept>
      <concept id="2913098736709465755" name="jetbrains.mps.build.structure.BuildLayout_ExportAsJavaLibrary" flags="ng" index="3dmp56">
        <reference id="2913098736709465758" name="library" index="3dmp53" />
      </concept>
      <concept id="5617550519002745364" name="jetbrains.mps.build.structure.BuildLayout" flags="ng" index="1l3spV" />
      <concept id="5617550519002745363" name="jetbrains.mps.build.structure.BuildProject" flags="ng" index="1l3spW">
        <property id="5204048710541015587" name="internalBaseDirectory" index="2DA0ip" />
        <child id="4796668409958418110" name="scriptsDir" index="auvoZ" />
        <child id="6647099934206700656" name="plugins" index="10PD9s" />
        <child id="7389400916848080626" name="parts" index="3989C9" />
        <child id="3542413272732620719" name="aspects" index="1hWBAP" />
        <child id="5617550519002745381" name="dependencies" index="1l3spa" />
        <child id="5617550519002745378" name="macros" index="1l3spd" />
        <child id="5617550519002745372" name="layout" index="1l3spN" />
      </concept>
      <concept id="8654221991637384182" name="jetbrains.mps.build.structure.BuildFileIncludesSelector" flags="ng" index="3qWCbU">
        <property id="8654221991637384184" name="pattern" index="3qWCbO" />
      </concept>
      <concept id="4701820937132281259" name="jetbrains.mps.build.structure.BuildCustomWorkflow" flags="ng" index="1y0Vig">
        <child id="4701820937132281260" name="parts" index="1y0Vin" />
      </concept>
      <concept id="4701820937132344003" name="jetbrains.mps.build.structure.BuildLayout_Container" flags="ng" index="1y1bJS">
        <child id="7389400916848037006" name="children" index="39821P" />
      </concept>
      <concept id="5248329904287794596" name="jetbrains.mps.build.structure.BuildInputFiles" flags="ng" index="3LXTmp">
        <child id="5248329904287794598" name="dir" index="3LXTmr" />
        <child id="5248329904287794679" name="selectors" index="3LXTna" />
      </concept>
      <concept id="4903714810883702019" name="jetbrains.mps.build.structure.BuildTextStringPart" flags="ng" index="3Mxwew">
        <property id="4903714810883755350" name="text" index="3MwjfP" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="0cf935df-4699-4e9c-a132-fa109541cba3" name="jetbrains.mps.build.mps">
      <concept id="1265949165890536423" name="jetbrains.mps.build.mps.structure.BuildMpsLayout_ModuleJars" flags="ng" index="L2wRC">
        <reference id="1265949165890536425" name="module" index="L2wRA" />
      </concept>
      <concept id="868032131020265945" name="jetbrains.mps.build.mps.structure.BuildMPSPlugin" flags="ng" index="3b7kt6" />
      <concept id="5253498789149381388" name="jetbrains.mps.build.mps.structure.BuildMps_Module" flags="ng" index="3bQrTs">
        <child id="5253498789149547825" name="sources" index="3bR31x" />
        <child id="5253498789149547704" name="dependencies" index="3bR37C" />
      </concept>
      <concept id="5253498789149585690" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleDependencyOnModule" flags="ng" index="3bR9La">
        <property id="5253498789149547713" name="reexport" index="3bR36h" />
        <reference id="5253498789149547705" name="module" index="3bR37D" />
      </concept>
      <concept id="5507251971038816436" name="jetbrains.mps.build.mps.structure.BuildMps_Generator" flags="ng" index="1yeLz9" />
      <concept id="4278635856200817744" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleModelRoot" flags="ng" index="1BupzO">
        <property id="8137134783396907368" name="convert2binary" index="1Hdu6h" />
        <property id="8137134783396676838" name="extracted" index="1HemKv" />
        <property id="2889113830911481881" name="deployFolderName" index="3ZfqAx" />
        <child id="8137134783396676835" name="location" index="1HemKq" />
      </concept>
      <concept id="3189788309731840247" name="jetbrains.mps.build.mps.structure.BuildMps_Solution" flags="ng" index="1E1JtA" />
      <concept id="3189788309731840248" name="jetbrains.mps.build.mps.structure.BuildMps_Language" flags="ng" index="1E1JtD">
        <child id="9200313594498201639" name="generator" index="1TViLv" />
      </concept>
      <concept id="322010710375871467" name="jetbrains.mps.build.mps.structure.BuildMps_AbstractModule" flags="ng" index="3LEN3z">
        <property id="8369506495128725901" name="compact" index="BnDLt" />
        <property id="322010710375892619" name="uuid" index="3LESm3" />
        <child id="322010710375956261" name="path" index="3LF7KH" />
      </concept>
      <concept id="7259033139236285166" name="jetbrains.mps.build.mps.structure.BuildMps_ExtractedModuleDependency" flags="nn" index="1SiIV0">
        <child id="7259033139236285167" name="dependency" index="1SiIV1" />
      </concept>
    </language>
  </registry>
  <node concept="1l3spW" id="gdy7DbXzKE">
    <property role="2DA0ip" value="../.." />
    <property role="TrG5h" value="mvn-install-myModel" />
    <node concept="1y0Vig" id="gdy7DbYXHn" role="1hWBAP">
      <node concept="3bMsLL" id="3nbByiJYQzn" role="1y0Vin">
        <ref role="3bMsLK" to="arit:6l_Qx579h0X" resolve="build" />
        <node concept="2VaFvH" id="6MJDQwbfkEf" role="3bMsLN">
          <property role="TrG5h" value="mvn-install" />
          <node concept="2Vbh7Z" id="6MJDQwbniYM" role="2VaTZU">
            <node concept="2pNNFK" id="6MJDQwbniZf" role="2Vbh7K">
              <property role="2pNNFO" value="echo" />
              <node concept="2pNUuL" id="6MJDQwbniZj" role="2pNNFR">
                <property role="2pNUuO" value="message" />
                <node concept="2pMdtt" id="6MJDQwbniZk" role="2pMdts">
                  <property role="2pMdty" value="running mvn-install" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2Vbh7Z" id="6MJDQwbfkEp" role="2VaTZU">
            <node concept="2pNNFK" id="6MJDQwbfkEt" role="2Vbh7K">
              <property role="2pNNFO" value="exec" />
              <node concept="2pNNFK" id="6MJDQwbpSEB" role="3o6s8t">
                <property role="2pNNFO" value="arg" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6MJDQwbpSEU" role="2pNNFR">
                  <property role="2pNUuO" value="value" />
                  <node concept="2pMdtt" id="6MJDQwbpSEV" role="2pMdts">
                    <property role="2pMdty" value="/c" />
                  </node>
                </node>
              </node>
              <node concept="2pNNFK" id="6MJDQwbpSEZ" role="3o6s8t">
                <property role="2pNNFO" value="arg" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6MJDQwbpSF0" role="2pNNFR">
                  <property role="2pNUuO" value="line" />
                  <node concept="2pMdtt" id="6MJDQwbpSF1" role="2pMdts">
                    <property role="2pMdty" value="mvn install:install-file" />
                  </node>
                </node>
              </node>
              <node concept="2pNNFK" id="6MJDQwbpSFl" role="3o6s8t">
                <property role="2pNNFO" value="arg" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6MJDQwbpSFm" role="2pNNFR">
                  <property role="2pNUuO" value="value" />
                  <node concept="2pMdtt" id="6MJDQwbpSFn" role="2pMdts">
                    <property role="2pMdty" value="-Dfile=${solutionName}.jar" />
                  </node>
                </node>
              </node>
              <node concept="2pNNFK" id="6MJDQwbqj$u" role="3o6s8t">
                <property role="2pNNFO" value="arg" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6MJDQwbqj$v" role="2pNNFR">
                  <property role="2pNUuO" value="value" />
                  <node concept="2pMdtt" id="6MJDQwbqj$w" role="2pMdts">
                    <property role="2pMdty" value="-Dsources=${solutionName}-src.jar" />
                  </node>
                </node>
              </node>
              <node concept="2pNNFK" id="6MJDQwbqj$V" role="3o6s8t">
                <property role="2pNNFO" value="arg" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6MJDQwbqj$W" role="2pNNFR">
                  <property role="2pNUuO" value="value" />
                  <node concept="2pMdtt" id="6MJDQwbqj$X" role="2pMdts">
                    <property role="2pMdty" value="-DgroupId=sk.tuke.ytm" />
                  </node>
                </node>
              </node>
              <node concept="2pNNFK" id="6MJDQwbqj_r" role="3o6s8t">
                <property role="2pNNFO" value="arg" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6MJDQwbqj_s" role="2pNNFR">
                  <property role="2pNUuO" value="value" />
                  <node concept="2pMdtt" id="6MJDQwbqj_t" role="2pMdts">
                    <property role="2pMdty" value="-DartifactId=mps-${solutionName}" />
                  </node>
                </node>
              </node>
              <node concept="2pNNFK" id="6MJDQwbqj_Y" role="3o6s8t">
                <property role="2pNNFO" value="arg" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6MJDQwbqj_Z" role="2pNNFR">
                  <property role="2pNUuO" value="value" />
                  <node concept="2pMdtt" id="6MJDQwbqjA0" role="2pMdts">
                    <property role="2pMdty" value="-Dversion=1.0-SNAPSHOT" />
                  </node>
                </node>
              </node>
              <node concept="2pNNFK" id="6MJDQwbqjA$" role="3o6s8t">
                <property role="2pNNFO" value="arg" />
                <property role="qg3DV" value="true" />
                <node concept="2pNUuL" id="6MJDQwbqjA_" role="2pNNFR">
                  <property role="2pNUuO" value="value" />
                  <node concept="2pMdtt" id="6MJDQwbqjAA" role="2pMdts">
                    <property role="2pMdty" value="-Dpackaging=jar" />
                  </node>
                </node>
              </node>
              <node concept="2pNUuL" id="6MJDQwbfkH9" role="2pNNFR">
                <property role="2pNUuO" value="dir" />
                <node concept="2pMdtt" id="6MJDQwbfkHa" role="2pMdts">
                  <property role="2pMdty" value="./build/artifacts/mvn-install-metamps" />
                </node>
              </node>
              <node concept="2pNUuL" id="6MJDQwbfkEP" role="2pNNFR">
                <property role="2pNUuO" value="executable" />
                <node concept="2pMdtt" id="6MJDQwbfkEQ" role="2pMdts">
                  <property role="2pMdty" value="cmd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="398rNT" id="6MJDQwbd1R0" role="1l3spd">
      <property role="TrG5h" value="mpsHome" />
      <node concept="55IIr" id="6MJDQwbd1R2" role="398pKh">
        <node concept="2Ry0Ak" id="6MJDQwbd1R5" role="iGT6I">
          <property role="2Ry0Am" value=".." />
          <node concept="2Ry0Ak" id="6MJDQwbd1Ra" role="2Ry0An">
            <property role="2Ry0Am" value=".." />
            <node concept="2Ry0Ak" id="6MJDQwbd1Rf" role="2Ry0An">
              <property role="2Ry0Am" value=".." />
              <node concept="2Ry0Ak" id="6MJDQwbd1Rk" role="2Ry0An">
                <property role="2Ry0Am" value=".." />
                <node concept="2Ry0Ak" id="6MJDQwbd1Rp" role="2Ry0An">
                  <property role="2Ry0Am" value=".." />
                  <node concept="2Ry0Ak" id="6MJDQwbd1Ru" role="2Ry0An">
                    <property role="2Ry0Am" value=".." />
                    <node concept="2Ry0Ak" id="6MJDQwbd1Rz" role="2Ry0An">
                      <property role="2Ry0Am" value=".." />
                      <node concept="2Ry0Ak" id="6MJDQwbd1RC" role="2Ry0An">
                        <property role="2Ry0Am" value="Jetbrains Toolbox" />
                        <node concept="2Ry0Ak" id="6MJDQwbd1RH" role="2Ry0An">
                          <property role="2Ry0Am" value="apps" />
                          <node concept="2Ry0Ak" id="6MJDQwbd1RM" role="2Ry0An">
                            <property role="2Ry0Am" value="MPS" />
                            <node concept="2Ry0Ak" id="6MJDQwbd1RR" role="2Ry0An">
                              <property role="2Ry0Am" value="ch-1" />
                              <node concept="2Ry0Ak" id="6MJDQwbd1RY" role="2Ry0An">
                                <property role="2Ry0Am" value="193.1270" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2kB4xC" id="6KhIIyUSBuk" role="1l3spd">
      <property role="TrG5h" value="solutionName" />
      <node concept="aVJcg" id="6KhIIyUSBuo" role="aVJcv">
        <node concept="NbPM2" id="6KhIIyUSBun" role="aVJcq">
          <node concept="3Mxwew" id="6KhIIyUSBum" role="3MwsjC">
            <property role="3MwjfP" value="myModel" />
          </node>
        </node>
      </node>
    </node>
    <node concept="PiPfp" id="gdy7DbXEw1" role="3989C9">
      <property role="TrG5h" value="myModel" />
    </node>
    <node concept="1E1JtA" id="gdy7DbYxaz" role="3989C9">
      <property role="BnDLt" value="true" />
      <property role="TrG5h" value="MySolution" />
      <property role="3LESm3" value="30397ffc-4623-4052-b0b9-54db4974cda4" />
      <node concept="55IIr" id="gdy7DbYxa_" role="3LF7KH">
        <node concept="2Ry0Ak" id="6TvRWUPkkX8" role="iGT6I">
          <property role="2Ry0Am" value="solutions" />
          <node concept="2Ry0Ak" id="6TvRWUPkkXd" role="2Ry0An">
            <property role="2Ry0Am" value="MySolution" />
            <node concept="2Ry0Ak" id="6TvRWUPkkXi" role="2Ry0An">
              <property role="2Ry0Am" value="MySolution.msd" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1BupzO" id="gdy7DbYxaK" role="3bR31x">
        <property role="3ZfqAx" value="models" />
        <property role="1Hdu6h" value="true" />
        <property role="1HemKv" value="true" />
        <node concept="3LXTmp" id="6TvRWUPkkXF" role="1HemKq">
          <node concept="55IIr" id="6TvRWUPkkXB" role="3LXTmr">
            <node concept="2Ry0Ak" id="6TvRWUPkkXC" role="iGT6I">
              <property role="2Ry0Am" value="solutions" />
              <node concept="2Ry0Ak" id="6TvRWUPkkXD" role="2Ry0An">
                <property role="2Ry0Am" value="MySolution" />
                <node concept="2Ry0Ak" id="6TvRWUPkkXE" role="2Ry0An">
                  <property role="2Ry0Am" value="models" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3qWCbU" id="6TvRWUPkkXG" role="3LXTna">
            <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
          </node>
        </node>
      </node>
      <node concept="1SiIV0" id="gdy7DbYxc6" role="3bR37C">
        <node concept="3bR9La" id="gdy7DbYxc7" role="1SiIV1">
          <property role="3bR36h" value="true" />
          <ref role="3bR37D" node="gdy7DbYxbk" resolve="MyLanguage" />
        </node>
      </node>
      <node concept="1SiIV0" id="gdy7DbYC$4" role="3bR37C">
        <node concept="3bR9La" id="gdy7DbYC$5" role="1SiIV1">
          <ref role="3bR37D" to="ffeo:78GwwOvB3tw" resolve="jetbrains.mps.ide.build" />
        </node>
      </node>
    </node>
    <node concept="1E1JtD" id="gdy7DbYxbk" role="3989C9">
      <property role="BnDLt" value="true" />
      <property role="TrG5h" value="MyLanguage" />
      <property role="3LESm3" value="340b79db-f74f-4ba0-8875-4d776d5e3a88" />
      <node concept="55IIr" id="gdy7DbYxbm" role="3LF7KH">
        <node concept="2Ry0Ak" id="gdy7DbYxbQ" role="iGT6I">
          <property role="2Ry0Am" value="languages" />
          <node concept="2Ry0Ak" id="gdy7DbYxbV" role="2Ry0An">
            <property role="2Ry0Am" value="MyLanguage" />
            <node concept="2Ry0Ak" id="gdy7DbYxc0" role="2Ry0An">
              <property role="2Ry0Am" value="MyLanguage.mpl" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1yeLz9" id="gdy7DbYxc4" role="1TViLv">
        <property role="TrG5h" value="MyLanguage#01" />
        <property role="3LESm3" value="fef1b653-aeda-4b90-ab50-791afd468a9f" />
        <node concept="1BupzO" id="6TvRWUPkkXT" role="3bR31x">
          <property role="3ZfqAx" value="generator/templates" />
          <property role="1Hdu6h" value="true" />
          <property role="1HemKv" value="true" />
          <node concept="3LXTmp" id="6TvRWUPkkXU" role="1HemKq">
            <node concept="55IIr" id="6TvRWUPkkXO" role="3LXTmr">
              <node concept="2Ry0Ak" id="6TvRWUPkkXP" role="iGT6I">
                <property role="2Ry0Am" value="languages" />
                <node concept="2Ry0Ak" id="6TvRWUPkkXQ" role="2Ry0An">
                  <property role="2Ry0Am" value="MyLanguage" />
                  <node concept="2Ry0Ak" id="6TvRWUPkkXR" role="2Ry0An">
                    <property role="2Ry0Am" value="generator" />
                    <node concept="2Ry0Ak" id="6TvRWUPkkXS" role="2Ry0An">
                      <property role="2Ry0Am" value="templates" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3qWCbU" id="6TvRWUPkkXV" role="3LXTna">
              <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1BupzO" id="6TvRWUPkkXL" role="3bR31x">
        <property role="3ZfqAx" value="models" />
        <property role="1Hdu6h" value="true" />
        <property role="1HemKv" value="true" />
        <node concept="3LXTmp" id="6TvRWUPkkXM" role="1HemKq">
          <node concept="55IIr" id="6TvRWUPkkXH" role="3LXTmr">
            <node concept="2Ry0Ak" id="6TvRWUPkkXI" role="iGT6I">
              <property role="2Ry0Am" value="languages" />
              <node concept="2Ry0Ak" id="6TvRWUPkkXJ" role="2Ry0An">
                <property role="2Ry0Am" value="MyLanguage" />
                <node concept="2Ry0Ak" id="6TvRWUPkkXK" role="2Ry0An">
                  <property role="2Ry0Am" value="models" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3qWCbU" id="6TvRWUPkkXN" role="3LXTna">
            <property role="3qWCbO" value="**/*.mps, **/*.mpsr, **/.model" />
          </node>
        </node>
      </node>
    </node>
    <node concept="55IIr" id="gdy7DbXzKF" role="auvoZ" />
    <node concept="1l3spV" id="gdy7DbXzKG" role="1l3spN">
      <node concept="3dmp56" id="gdy7DbY2Ki" role="39821P">
        <ref role="3dmp53" node="gdy7DbXEw1" resolve="myModel" />
        <node concept="L2wRC" id="gdy7DbYCwX" role="39821P">
          <ref role="L2wRA" node="gdy7DbYxaz" resolve="MySolution" />
        </node>
      </node>
    </node>
    <node concept="3b7kt6" id="gdy7DbXzLz" role="10PD9s" />
    <node concept="10PD9b" id="gdy7DbXzLC" role="10PD9s" />
    <node concept="2sgV4H" id="gdy7DbYC3O" role="1l3spa">
      <ref role="1l3spb" to="ffeo:3IKDaVZmzS6" resolve="mps" />
      <node concept="398BVA" id="gdy7DbYC5A" role="2JcizS">
        <ref role="398BVh" node="6MJDQwbd1R0" resolve="mpsHome" />
      </node>
    </node>
  </node>
</model>

